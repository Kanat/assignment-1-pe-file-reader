///@file pe_file.h
///@brief Header file containing structures and functions for working with Portable Executable (PE) files.

#pragma once
#ifndef PE_FILE_H
#define PE_FILE_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIGNATURE_OFFSET 0x3c ///< Offset of the PE signature in the file header.
#define PE_SIGNATURE 0x4550   ///< PE signature value.

#ifdef _MSC_VER
#pragma pack(push, 1)
#endif

/// @struct PEFileHeader
/// @brief Represents the file header of a PE file.
struct
#if defined __clang__ || defined __GNUC__
        __attribute__((packed))
#endif
PEFileHeader {
    uint32_t signature;               ///< PE signature value.
    uint16_t machine;                 ///< Target machine type.
    uint16_t number_of_sections;      ///< Number of sections in the file.
    uint32_t time_date_stamp;         ///< Date and time of file creation.
    uint32_t pointer_to_symbol_table; ///< Pointer to symbol table.
    uint32_t number_of_symbols;       ///< Number of symbols.
    uint16_t size_of_optional_header; ///< Size of optional header.
    uint16_t characteristics;         ///< File characteristics.
};

/// @struct PESectionHeader
/// @brief Represents a section header in a PE file.
struct
#if defined __clang__ || defined __GNUC__
        __attribute__((packed))
#endif
PESectionHeader {
    uint8_t name[8];                 ///< Section name.
    uint32_t virtual_size;           ///< Size of section in memory.
    uint32_t virtual_address;        ///< Address of section in memory.
    uint32_t size_of_raw_data;       ///< Size of section in file.
    uint32_t pointer_to_raw_data;    ///< Pointer to section data in file.
    uint32_t pointer_to_relocations; ///< Pointer to relocation entries.
    uint32_t pointer_to_linenumbers; ///< Pointer to line number entries.
    uint16_t number_of_relocations;  ///< Number of relocation entries.
    uint16_t number_of_linenumbers;  ///< Number of line number entries.
    uint32_t characteristics;        ///< Section characteristics.
};

/// @struct PEFile
/// @brief Represents a PE file.
struct
#if defined __clang__ || defined __GNUC__
        __attribute__((packed))
#endif
PEFile {
    struct PEFileHeader header;      ///< File header.
    struct PESectionHeader *section; ///< Pointer to array of section headers.
};

#ifdef _MSC_VER
#pragma pack(pop)
#endif

/// @enum read_status
/// @brief Represents the status of reading a PE file.
enum read_status {
    READ_OK = 0, ///< Read operation completed successfully.
    READ_ERROR   ///< Read operation encountered an error.
};

/// @enum write_status
/// @brief Represents the status of writing to a file.
enum write_status {
    WRITE_OK = 0, ///< Write operation completed successfully.
    WRITE_ERROR   ///< Write operation encountered an error.
};

/// @brief Reads a PE file from disk and populates a PEFile struct with its contents.
/// @param file The file to read the PE header and section headers from.
/// @param pe_file The PE file structure to store the read data in.
/// @return read_status value indicating whether file was read successfully.
enum read_status read_pe_file(FILE *file, struct PEFile *pe_file);

/// @brief Finds the section header of a PE file with a given name.
/// @param pe_file The PE file to search for the section header in.
/// @param section_name The name of the section header to find.
/// @return The PE section header with the given name, or a section header with all fields set to zero if not found.
struct PESectionHeader find_section_header_by_name(struct PEFile *pe_file, char *section_name);

/// @brief Writes the data of a section to a file.
/// @param input_file The file to read the section data from.
/// @param output_file The file to write the section data to.
/// @param section_header The PE section header of the section to write.
/// @return write_status value indicating whether data was written successfully.
enum write_status write_section_data_to_file(FILE *input_file, FILE *output_file, struct PESectionHeader section_header);

#endif
