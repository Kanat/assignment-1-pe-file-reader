/// @file main.c
/// @brief Main application file. A program that implements reading a PE file and outputting one of the sections to a file.

#include <../include/pe_file.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *file) { fprintf(file, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n"); }

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char **argv) {
    if (argc != 4) {
        usage(stdout);
        return 1;
    }
    struct PEFile *pe_file = malloc(sizeof(struct PEFile));
    if (!pe_file) {
        printf("Memory allocation error for PE file!\n");
        return 1;
    }
    FILE *input_file = fopen(argv[1], "rt");
    if (!input_file) {
        free(pe_file);
        printf("PE file opening error!\n");
        return 1;
    }
    if (read_pe_file(input_file, pe_file) != READ_OK) {
        free(pe_file);
        fclose(input_file);
        return 1;
    }
    struct PESectionHeader found_section_header = find_section_header_by_name(pe_file, argv[2]);
    if (found_section_header.pointer_to_raw_data == 0) {
        free(pe_file->section);
        free(pe_file);
        fclose(input_file);
        return 1;
    }
    FILE *output_file = fopen(argv[3], "wb");
    if (!output_file) {
        free(pe_file->section);
        free(pe_file);
        fclose(input_file);
        printf("Output file opening error!\n");
        return 1;
    }
    if (write_section_data_to_file(input_file, output_file, found_section_header) != WRITE_OK) {
        free(pe_file->section);
        free(pe_file);
        fclose(input_file);
        fclose(output_file);
        return 1;
    }
    free(pe_file->section);
    free(pe_file);
    fclose(input_file);
    fclose(output_file);
    return 0;
}
