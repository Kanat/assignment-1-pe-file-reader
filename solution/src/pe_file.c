///@file pe_file.c
///@brief Header file containing structures and functions for working with Portable Executable (PE) files.

#include <../include/pe_file.h>

/// @brief Reads a PE file from disk and populates a PEFile struct with its contents.
/// @param file The file to read the PE header and section headers from.
/// @param pe_file The PE file structure to store the read data in.
/// @return read_status value indicating whether file was read successfully.
enum read_status read_pe_file(FILE *file, struct PEFile *pe_file) {
    if (!file) {
        printf("File opening error!\n");
        return READ_ERROR;
    }
    fseek(file, SIGNATURE_OFFSET, SEEK_SET);
    uint32_t offset;
    if (fread(&offset, 4, 1, file) != 1) {
        printf("Error reading signature!\n");
        return READ_ERROR;
    }
    fseek(file, offset, SEEK_SET);
    if (fread(&pe_file->header, sizeof(pe_file->header), 1, file) != 1) {
        printf("Error reading PE file header!\n");
        return READ_ERROR;
    }
    if (pe_file->header.signature != PE_SIGNATURE) {
        printf("The file does not match the PE signature!\n");
        return READ_ERROR;
    }
    pe_file->section = malloc(sizeof(struct PESectionHeader) * pe_file->header.number_of_sections);
    if (!pe_file->section) {
        printf("Memory allocation error for PE section header!\n");
        return READ_ERROR;
    }
    fseek(file, pe_file->header.size_of_optional_header, SEEK_CUR);
    if (fread(pe_file->section, sizeof(struct PESectionHeader), pe_file->header.number_of_sections, file) !=
        pe_file->header.number_of_sections) {
        free(pe_file->section);
        printf("Error reading PE section header!\n");
        return READ_ERROR;
    }
    return READ_OK;
}


/// @brief Finds the section header of a PE file with a given name.
/// @param pe_file The PE file to search for the section header in.
/// @param section_name The name of the section header to find.
/// @return The PE section header with the given name, or a section header with all fields set to zero if not found.
struct PESectionHeader find_section_header_by_name(struct PEFile *pe_file, char *section_name) {
    for (uint16_t i = 0; i < pe_file->header.number_of_sections; i++) {
        if (memcmp(pe_file->section[i].name, section_name, strlen(section_name)) == 0) {
            return pe_file->section[i];
        }
    }
    printf("Not found!\n");
    return (struct PESectionHeader) {0};
}


/// @brief Writes the data of a section to a file.
/// @param input_file The file to read the section data from.
/// @param output_file The file to write the section data to.
/// @param section_header The PE section header of the section to write.
/// @return write_status value indicating whether data was written successfully.
enum write_status
write_section_data_to_file(FILE *input_file, FILE *output_file, struct PESectionHeader section_header) {
    if (!input_file || !output_file) {
        printf("File oppening error!\n");
        return WRITE_ERROR;
    }
    fseek(input_file, section_header.pointer_to_raw_data, SEEK_SET);
    char *section_data = malloc(section_header.size_of_raw_data);
    if (!section_data) {
        printf("Memory allocation error for PE section data!\n");
        return WRITE_ERROR;
    }
    if (fread(section_data, section_header.size_of_raw_data, 1, input_file) != 1) {
        free(section_data);
        printf("Error reading PE section data!\n");
        return WRITE_ERROR;
    }
    if (fwrite(section_data, section_header.size_of_raw_data, 1, output_file) != 1) {
        free(section_data);
        printf("Error writing to output file\n");
        return WRITE_ERROR;
    }
    free(section_data);
    return WRITE_OK;
}
